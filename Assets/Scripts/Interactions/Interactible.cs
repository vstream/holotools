﻿using UnityEngine;
using UnityEngine.Events;

public class Interactible : MonoBehaviour {
    public UnityEvent onSelect; 

	void GazeEntered() {
        
    }

    void GazeExited() {

    }

    void OnSelect() {
        if(onSelect != null) {
            onSelect.Invoke();
        } else {
            Debug.Log("No click callback set");
        }
    }
}
