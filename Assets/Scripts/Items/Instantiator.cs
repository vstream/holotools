﻿using UnityEngine;

public class Instantiator : Singleton<Instantiator> {

    ItemLocations locations;

    [System.Serializable]
    public struct ObjectReference {
        public string Title;
        public GameObject Model;
    }

    public GameObject loadWorldMenu;

    [Header("Models")]
    public ObjectReference[] models;

    public void DestroyMe(GameObject g) {
        DestroyImmediate(g);
        UpdateItemLocations();
    }

    public void ShowLoadWorldMenu() {
        if (canEdit) {
            loadWorldMenu.SetActive(true);
        }
    }

    public void HideLoadWorldMenu() {
        if (canEdit) {
            loadWorldMenu.SetActive(false);
        }
    }

    public void ToggleLoadWorldMenu() {
        if (canEdit) {
            loadWorldMenu.SetActive(!loadWorldMenu.activeSelf);
            if (loadWorldMenu.activeSelf) {
                LoadMenu.Instance.BuildMenu();
            }
        }
    }

    public void LoadWorld() {
        Debug.Log("Loading World");
        DestroyObjects();

        Debug.Log("Loading Items from json");
        locations = ItemLocations.LoadFromJSON();

        CreateObjects();
    }

    void DestroyObjects() {
        Debug.Log("Destroying Everything");
        foreach (Transform child in WorldParent.Instance.gameObject.transform) {
            if (child.gameObject.name != "Control") {
                Debug.Log("Exterminate!");
                Destroy(child.gameObject);
            }
        }
    }

    [Header("Edit")]
    public bool canEdit = true;
    public void EditMode() {
        canEdit = !canEdit;
        ToyBox.Instance.gameObject.SetActive(canEdit);
    }

    public void UpdateItemLocations() {
        Debug.Log("Updating Item Locations");

        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");

        ItemLocations loc = new ItemLocations();
        for(int i = 0; i < items.Length; i++) {
            loc.AddItem(items[i].GetComponent<Item_ID>().item);
        }

        locations = loc;
        //ItemLocations.WriteToJSON(locations);
    }

    public void SaveItemLocations() {
        if (canEdit) {
            ItemLocations.WriteToJSON(locations);
        }
    }


    void Start() {
        Debug.Log("Loading Items from json");
        locations = ItemLocations.LoadFromJSON();

        CreateObjects();

        EditMode();
    }

    public void ReloadItems(ItemLocations il) {
        DestroyObjects();
        locations = il;
        CreateObjects();
        HideLoadWorldMenu();
    }

    GameObject GetObjectReferenceByName(string name) {
        GameObject g = null;
        for(int i = 0; i < models.Length; i++) {
            if(models[i].Title == name) {
                g = models[i].Model;
            }
        }
        return g;
    }

    void CreateObjects() {
        Debug.Log("Creating Objects");
        for (int i = 0; i < locations.items.Count; i++) {
            Debug.Log("Creating Object " + (i+1));

            GameObject prefab = GetObjectReferenceByName(locations.items[i].name);
            if (prefab == null) {
                Debug.Log("No Model in database matches name");
                return;
            }

            GameObject g = Instantiate(prefab, WorldParent.Instance.gameObject.transform, false);
            g.transform.localPosition = locations.items[i].position;
            g.transform.localRotation = Quaternion.Euler(locations.items[i].rotation);
            g.transform.localScale = locations.items[i].scale;
            g.GetComponent<Item_ID>().item = locations.items[i];
            Item.CheckID(locations.items[i].id);
        }
    }

    ObjectReference GetObjectReference(string t) {
        for(int i = 0; i < models.Length; i++) {
            if(models[i].Title == t) {
                return models[i];
            }
        }
        Debug.Log("Couldn't find object by title, returning new");
        return new ObjectReference();
    }

    public void OnCreate(string obj) {
        ObjectReference or = GetObjectReference(obj);
        if (or.Title != null && or.Title != "") {
            OnCreate(or);
        }
    }

    public void OnCreate(ObjectReference or) {
        GameObject g = Instantiate(or.Model, WorldParent.Instance.transform, false);
        g.transform.localPosition = new Vector3(0, 0, 0);
        Item i = new Item(or.Title, new Vector3(0, 0, 0));

        Debug.Log("Add the Item");
        locations.AddItem( i );
        
        g.GetComponent<Item_ID>().item = i;
        g.GetComponent<Placeable>().placing = true;
    }
	
}
