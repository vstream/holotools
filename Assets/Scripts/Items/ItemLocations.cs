﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;

[System.Serializable]
public class ItemLocations {
    
    public List<Item> items = new List<Item>();
    
    public static ItemLocations LoadFromJSON() {
        string[] saves = LoadMenu.GetAllSaves();
        string filepath = "item_locations.json";

        Debug.Log("Save count: " + saves.Length);
        if (saves.Length > 0) {
            filepath = saves[saves.Length - 1];
        }

        if (File.Exists(Application.streamingAssetsPath + "/save_data/" + filepath)) {
            string json = ReadFromFile("save_data/" + filepath);
            return JsonUtility.FromJson<ItemLocations>(json);
        } else {
            return new ItemLocations();
        }
    }

    public static void WriteToJSON(ItemLocations il) {
        string json = JsonUtility.ToJson(il, true);
        try {
            Debug.Log("Attempting to save");
            File.WriteAllText(
                Application.streamingAssetsPath + "/save_data/items_" + DateTime.Now.ToString("HHmmssddMMyy") + ".json",
                json
            );
            Debug.Log("Save Successful");
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    public static void WriteToFile(string file, string json) {
        try {
            File.WriteAllText(
                Application.streamingAssetsPath + "/" + file,
                json
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    public static string ReadFromFile(string file) {
        string output = "";
        try {
            output = File.ReadAllText(
                Application.streamingAssetsPath + "/" + file
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
        return output;
    }

    public void AddItem(Item item) {
        if (item.name == "" || item.name == null) {
            Debug.Log("Some of the Item details were missing");
            return;
        }

        bool found = false;
        for (int i = 0; i < items.Count; i++) {
            if (items[i].id == item.id) {
                Debug.Log("Found Item in collection, updating location");
                found = true;
                items[i] = item;
            }
        }

        if (!found) {
            Debug.Log("Adding item to collection");
            items.Add(item);
        }
    }
}

[System.Serializable]
public class Item {
    static int count = 0;
    public int id = 0;
    public string name = "";
    public Vector3 position = new Vector3(0, 0, 0);
    public Vector3 rotation = new Vector3(0, 0, 0);
    public Vector3 scale    = new Vector3(1, 1, 1);

    public Item(string n, Vector3 p) {
        count++;
        id = count;
        name = n;
        position = p;
        rotation = new Vector3(0,0,0);
        scale = new Vector3(1,1,1);
    }

    public Item(string n, Vector3 p, Vector3 r, Vector3 s) {
        count++;
        id = count;
        name = n;
        position = p;
        rotation = r;
        scale = s;
    }

    public static void CheckID(int id) {
        if(count < id) {
            count = id;
        }
    }
}