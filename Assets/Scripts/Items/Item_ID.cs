﻿using UnityEngine;

public class Item_ID : MonoBehaviour {
    public Item item;

    public void UpdateMe() {
        item.position = transform.localPosition;
        item.rotation = transform.localRotation.eulerAngles;
        item.scale = transform.localScale;
        Instantiator.Instance.UpdateItemLocations();
    }
}
