﻿using UnityEngine;
using HoloToolkit.Unity;

public class Placeable : MonoBehaviour {

    public bool placing = false;
    static float distance = 3f;
    private Material[] defaultMaterials;

    void Start() {
        defaultMaterials = GetComponent<Renderer>().materials;
    }

    void Update() {
        if (placing) {
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(
                Camera.main.transform.position, 
                Camera.main.transform.forward, 
                out hitInfo, 
                distance,
                SpatialMappingManager.Instance.LayerMask
            );

            Vector3 hitpoint;

            if (hit) {
                hitpoint = hitInfo.point - ( Camera.main.transform.position.normalized * 0.1f);
                transform.LookAt(Camera.main.transform);
            } else {
                hitpoint = Camera.main.transform.position + Camera.main.transform.forward * distance;
                transform.LookAt(Camera.main.transform);
            }
            transform.position = hitpoint;
        }
    }


    public void DeleteMe() {
        Debug.Log("Delete Me");
        Instantiator.Instance.DestroyMe(gameObject);
    }

    public void MoveMe() {
        placing = !placing;

        if (!placing) {
            GetComponent<Item_ID>().UpdateMe();
            for (int i = 0; i < defaultMaterials.Length; i++) {
                defaultMaterials[i].SetFloat("_Highlight", .25f);
            }
        } else {
            for (int i = 0; i < defaultMaterials.Length; i++) {
                defaultMaterials[i].SetFloat("_Highlight", 0.5f);
            }
        }
    }

    public void RotateMe() {
        Debug.Log("Rotate Me");
    }

    public void ScaleMe() {
        Debug.Log("Scale Me");
    }

    void GazeEntered() {
        Debug.Log("GazeEntered");
        for (int i = 0; i < defaultMaterials.Length; i++) {
            defaultMaterials[i].SetFloat("_Highlight", .25f);
        }
    }

    void GazeExited() {
        Debug.Log("GazeExited");
        for (int i = 0; i < defaultMaterials.Length; i++) {
            defaultMaterials[i].SetFloat("_Highlight", 0f);
        }
    }

    void OnSelect() {
        Debug.Log("OnSelect");
        MoveMe();
    }

}
