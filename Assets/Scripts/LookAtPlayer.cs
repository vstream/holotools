﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {
    Transform target;

	void Start () {
        target = Camera.main.transform;
	}
	
	void Update () {
        transform.LookAt(2 * transform.position - target.position);
	}
}
