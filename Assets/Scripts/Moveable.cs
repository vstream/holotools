﻿using HoloToolkit.Unity;
using UnityEngine;

public class Moveable : MonoBehaviour {

    public bool placing = false;
    static float distance = 3f;

    void Update() {
        if (placing) {
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(
                Camera.main.transform.position,
                Camera.main.transform.forward,
                out hitInfo,
                distance,
                SpatialMappingManager.Instance.LayerMask
            );

            Vector3 hitpoint;

            if (hit) {
                hitpoint = hitInfo.point - (Camera.main.transform.position.normalized * 0.1f);
                transform.LookAt(Camera.main.transform);
            } else {
                hitpoint = Camera.main.transform.position + Camera.main.transform.forward * distance;
                transform.LookAt(Camera.main.transform);
            }
            transform.position = hitpoint;
        }
    }

    public void MoveMe() {
        placing = !placing;
    }
    

    void OnSelect() {
        Debug.Log("OnSelect");
        MoveMe();
    }
}
