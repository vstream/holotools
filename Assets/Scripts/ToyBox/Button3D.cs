﻿using UnityEngine;
using UnityEngine.Events;

public class Button3D : MonoBehaviour {
    public BtnType type;
    public Placeable place;

	void OnSelect() {

        place.Invoke(type.ToString() + "Me", 0f);
    }

    void Awake() {
        place = GetComponentInParent<Placeable>();
    }


}

public enum BtnType { Delete, Rotate, Scale }
