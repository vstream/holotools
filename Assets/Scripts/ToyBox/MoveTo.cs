﻿using UnityEngine;

public class MoveTo : MonoBehaviour {

    public Transform moveToPosition;
    public string itemName;

	void Update () {
        if(moveToPosition != null) {
            transform.position = Vector3.Lerp(
                transform.position,
                moveToPosition.position,
                0.1f
            );
        }
    }

}
