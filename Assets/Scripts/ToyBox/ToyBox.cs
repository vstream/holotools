﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyBox : Singleton<ToyBox> {

    [Header("Items MoveTo scripts")]
    public List<MoveTo> items = new List<MoveTo>();

    [Header("Slots on the ToyBox")]
    public Transform[] slots;
    public Transform Left;
    public Transform Right;

    int index;

    void Awake() {
        index = 0;

        for (int i = 0; i < items.Count; i++) {
            if(i < slots.Length) {
                items[i].moveToPosition = slots[i].GetComponent<ToyBoxItem>().itemSlot;
            } else {
                items[i].moveToPosition = Right;
            }
        }
    }

    public void MoveLeft() {
        index--;
        boundIndex();
        UpdateThings();
    }

    public void MoveRight() {
        index++;
        boundIndex();
        UpdateThings();
    }

    void Start() {
        UpdateThings();
    }

    void UpdateThings() {
        for(int i = 0; i < items.Count; i++) {
            if (index > items.Count - 4) {

            } else if(i == index) {
                items[i].moveToPosition = slots[0].GetComponent<ToyBoxItem>().itemSlot;
                slots[0].GetComponent<ToyBoxItem>().SetItemName(items[i].itemName);
            } else if (i == index + 1) {
                items[i].moveToPosition = slots[1].GetComponent<ToyBoxItem>().itemSlot;
                slots[1].GetComponent<ToyBoxItem>().SetItemName(items[i].itemName);
            } else if (i == index + 2) {
                items[i].moveToPosition = slots[2].GetComponent<ToyBoxItem>().itemSlot;
                slots[2].GetComponent<ToyBoxItem>().SetItemName(items[i].itemName);
            } else if (i == index + 3) {
                items[i].moveToPosition = slots[3].GetComponent<ToyBoxItem>().itemSlot;
                slots[3].GetComponent<ToyBoxItem>().SetItemName(items[i].itemName);
            } else if (i < index) {
                items[i].moveToPosition = Left;
            } else if (i > index + 3) {
                items[i].moveToPosition = Right;
            }
        }
    }

    void boundIndex() {
        if(items.Count < 4) {
            index = items.Count;
        } else if (index > items.Count - 4) {
            index = items.Count - 4;
        } else if (index < 0) {
            index = 0;
        }
    }

}
