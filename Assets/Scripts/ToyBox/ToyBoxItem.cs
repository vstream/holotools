﻿using UnityEngine;
using UnityEngine.UI;

public class ToyBoxItem : MonoBehaviour {

    public string itemName = "";
    public Transform itemSlot;
    public Text label;

    public void SetItemName(string iName) {
        itemName = iName;
        label.text = itemName;
    }

    void OnSelect() {
        Debug.Log("OnSelect");
        Instantiator.Instance.OnCreate(itemName);
    }

    void GazeEntered() {
        Debug.Log("GazeEntered");
    }

    void GazeExited() {
        Debug.Log("GazeExited");
    }

}
