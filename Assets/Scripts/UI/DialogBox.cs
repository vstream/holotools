﻿using UnityEngine;
using UnityEngine.UI;

public class DialogBox : Singleton<DialogBox> {

    // The MessageBox
    public Text messageBox;

    // Dialog Callbacks
    GameObject callbackObject;
    string cbYes;
    string cbNo;
    string cbParam;

    public void Setup(string msg, GameObject g, string callbackYes, string callbackNo, string callbackParam) {
        gameObject.SetActive(true);
        Debug.Log("Message: " + msg);
        messageBox.text = msg;
        callbackObject = g;
        cbYes = callbackYes;
        cbNo = callbackNo;
        cbParam = callbackParam;
    }

    public void PressYes() {
        callbackObject.SendMessage(cbYes, cbParam);
        Reset();
    }

    public void PressNo() {
        callbackObject.SendMessage(cbNo);
        Reset();
    }

    public void Reset() {
        messageBox.text = "";
        callbackObject = null;
        cbYes = "";
        cbNo = "";
        gameObject.SetActive(false);
    }
}
