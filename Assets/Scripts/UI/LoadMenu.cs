﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LoadMenu : Singleton<LoadMenu> {

    public RectTransform listParent;
    public GameObject listItem;
    public GameObject noSaves;
    public GameObject dialogBox;
    private string path = "/save_data/";

    void Awake() {
        CreateSAFolder("save_data");
        // Load the json files from the saves folder
        path = Application.streamingAssetsPath + path;
        BuildMenu();
    }

    void CreateSAFolder(string folderName) {
        if(!File.Exists(Application.streamingAssetsPath + "/" + folderName)) {
            Directory.CreateDirectory(Application.streamingAssetsPath + "/" + folderName);
        }
    }

    void Start() {
        transform.parent.gameObject.SetActive(false);
    }

    public static string[] GetAllSaves() {
        string[] paths = Directory.GetFiles(Application.streamingAssetsPath + "/save_data/", "*.json", SearchOption.TopDirectoryOnly);
        for(int i = 0; i < paths.Length; i++) {
            paths[i] = Path.GetFileName(paths[i]);
        }
        return paths;
    }

    public void DeleteButton(string filename) {
        Debug.Log("Delete pressed");
        dialogBox.SetActive(true);
        DialogBox.Instance.Setup("Are you sure you want to delete " + filename + "?", 
            gameObject, "DeleteSave", "HideDialog", filename);
    }

    public void LoadButton(string filename) {
        Debug.Log("Load pressed");
        dialogBox.SetActive(true);
        DialogBox.Instance.Setup("Are you sure you want to load " + filename + "?", 
            gameObject, "LoadSave", "HideDialog", filename);
    }

    public void DeleteSave(string filename) {
        Debug.Log("Delete Save pressed");
        DeleteFile(filename);
    }

    void DeleteFile(string filename) {
        try {
            File.Delete(path + filename);
            BuildMenu();
        } catch(Exception e) {
            Debug.Log("Exception While Deleting File: " + e);
        }
    }

    public void LoadSave(string filename) {
        Debug.Log("Load Save pressed");
        ItemLocations il = LoadFromJSON(filename);
        Instantiator.Instance.ReloadItems(il);
    }

    public void HideDialog() {
        Debug.Log("Hide Dialog");
    }

    public void BuildMenu() {
        Debug.Log("Get All Saves called");
        string[] saves = GetAllSaves();

        Debug.Log("Clear Children called");
        listParent.ClearChildren();

        if(saves.Length == 0) {
            Debug.Log("No Saves");
            noSaves.SetActive(true);
        } else {
            Debug.Log("Creating " + saves.Length + " ListItems");
            noSaves.SetActive(false);
            for (int i = 0; i < saves.Length; i++) {
                string a = saves[i];
                string datestring = File.GetLastWriteTime(Application.streamingAssetsPath + "/save_data/" + saves[i]).ToString("dd/MM/yy HH:mm:ss");

                GameObject g = Instantiate(listItem, listParent.transform, false);
                g.transform.FindChild("Title_Date").GetComponent<Text>().text = a + "\n" + datestring;

                Button loadbtn = g.transform.FindChild("LoadBtn").GetComponent<Button>();
                loadbtn.onClick.AddListener(
                        () => LoadButton(a)
                );

                Button delbtn = g.transform.FindChild("DelBtn").GetComponent<Button>();
                delbtn.onClick.AddListener(
                    () => DeleteButton(a)
                );

            }
        }
    }

    public void SaveData(string filename, string json) {
        try {
            File.WriteAllText(
                path + filename,
                json
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }


    // Old Versions
    public static ItemLocations LoadFromJSON(string filename) {
        string json = ReadFromFile("save_data/" + filename);
        return JsonUtility.FromJson<ItemLocations>(json);
    }

    public static void WriteToJSON(ItemLocations il, string filename) {
        string json = JsonUtility.ToJson(il, true);
        try {
            File.WriteAllText(
                Application.streamingAssetsPath + "save_data/" + filename,
                json
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    public static void WriteToFile(string file, string json) {
        try {
            File.WriteAllText(
                Application.streamingAssetsPath + "/" + file,
                json
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    public static string ReadFromFile(string file) {
        string output = "";
        try {
            output = File.ReadAllText(
                Application.streamingAssetsPath + "/" + file
            );
        } catch (Exception e) {
            Debug.LogException(e);
        }
        return output;
    }

}
