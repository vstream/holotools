﻿using UnityEngine;

public static class Extensions  {

	public static void ClearChildren(this Transform trans) {
        Debug.Log("Destroying " + trans.childCount + " Children");
        for(int i = trans.childCount; i > 0; i--) {
            GameObject.Destroy( trans.GetChild(i - 1).gameObject );
        }
    }

}
