﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : Singleton<T> {
    private static T _Instance;
    public static T Instance {
        get {
            if (_Instance == null) {
                _Instance = FindObjectOfType<T>();
                if(_Instance == null) {
                    GameObject g = new GameObject();
                    g.AddComponent<T>();
                    _Instance = g.GetComponent<T>();
                }
            }
            return _Instance;
        }
    }
}
