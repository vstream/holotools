﻿using UnityEngine;
using UnityEngine.VR.WSA.Persistence;
using UnityEngine.VR.WSA;

public class WorldParent : Singleton<WorldParent> {

    // The name to use when storing this object
    public string ObjectAnchorStoreName;

    // The Anchor Store 
    WorldAnchorStore anchorStore;

    public float distance = 0f;

    bool Placing = false;

    void Start() {
        WorldAnchorStore.GetAsync(AnchorStoreReady);
    }

    void AnchorStoreReady(WorldAnchorStore store) {
        anchorStore = store;
        Placing = true;

        Debug.Log("looking for " + ObjectAnchorStoreName);
        string[] ids = anchorStore.GetAllIds();
        for (int index = 0; index < ids.Length; index++) {
            Debug.Log(ids[index]);
            if (ids[index] == ObjectAnchorStoreName) {
                anchorStore.Load(ids[index], gameObject);
                Placing = false;
                break;
            }
        }
        Debug.Log(Placing);
    }


    void Update() {
        if (Placing) {
            gameObject.transform.position = Camera.main.transform.position + Camera.main.transform.forward * distance;
        }
    }

    void OnSelect() {
        if (anchorStore == null) {
            return;
        }

        if (Placing) {
            WorldAnchor attachingAnchor = gameObject.AddComponent<WorldAnchor>();
            if (attachingAnchor.isLocated) {
                Debug.Log("Saving persisted position immediately");
                bool saved = anchorStore.Save(ObjectAnchorStoreName, attachingAnchor);
                Debug.Log("saved: " + saved);
            } else {
                attachingAnchor.OnTrackingChanged += AttachingAnchor_OnTrackingChanged;
            }
        } else {
            WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();
            if (anchor != null) {
                DestroyImmediate(anchor);
            }

            string[] ids = anchorStore.GetAllIds();
            for (int index = 0; index < ids.Length; index++) {
                Debug.Log(ids[index]);
                if (ids[index] == ObjectAnchorStoreName) {
                    bool deleted = anchorStore.Delete(ids[index]);
                    Debug.Log("deleted: " + deleted);
                    break;
                }
            }
        }

        Placing = !Placing;
    }

    private void AttachingAnchor_OnTrackingChanged(WorldAnchor self, bool located) {
        if (located) {
            Debug.Log("Saving persisted position in callback");
            bool saved = anchorStore.Save(ObjectAnchorStoreName, self);
            Debug.Log("saved: " + saved);
            self.OnTrackingChanged -= AttachingAnchor_OnTrackingChanged;
        }
    }

}
